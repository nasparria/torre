import React, { useState, useEffect } from "react";

function UserSkills(props) {
  const [skills, setSkills] = useState([]);

  useEffect(() => {
    const fetchData = async () => {
      const result = await fetch(`/user_skills/${props.username}`);
      const data = await result.json();
      const skillsArray = Object.entries(data).map(([key, value]) => ({
        name: key,
        value,
      }));
      setSkills(skillsArray);
    };
    fetchData();
  }, [props.username]);

  return (
    <div>
      <h2>Skills</h2>
      <ul>
        {skills.map((skill, index) => (
          <li key={index}>
            <strong>{skill.name}:</strong>{" "}
            {typeof skill.value === "string"
              ? skill.value
              : Object.entries(skill.value).map(([key, value], index) => (
                  <p key={index}>
                    <strong>{key}: </strong>
                    {value}
                  </p>
                ))}
          </li>
        ))}
      </ul>
    </div>
  );
}

export default UserSkills;
