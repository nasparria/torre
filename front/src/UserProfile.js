import React, { useState, useEffect } from 'react';
import { fetchUserData } from './api';
import StrengthDetails from './SkillsDetails';
import 'animate.css/animate.compat.css';
import styles from './UserProfile.module.css';

function groupAndSortStrengths(strengths) {
  const groupedStrengths = {};

  strengths.forEach((strength) => {
    if (!groupedStrengths[strength.proficiency]) {
      groupedStrengths[strength.proficiency] = [];
    }
    groupedStrengths[strength.proficiency].push(strength);
  });

  for (const proficiencyLevel in groupedStrengths) {
    groupedStrengths[proficiencyLevel].sort((a, b) =>
      a.name.localeCompare(b.name)
    );
  }

  return groupedStrengths;
}

function UserProfile({ username }) {
  const [userData, setUserData] = useState(null);
  const [loading, setLoading] = useState(true);
  const [error, setError] = useState(null);
  const [selectedStrength, setSelectedStrength] = useState(null);

  useEffect(() => {
    async function fetchData() {
      try {
        const data = await fetchUserData(username);
        console.log('Fetched data:', data); // Add this line to log the fetched data
        setUserData(data);
      } catch (e) {
        setError(e.message);
      }
      setLoading(false);
    }

    fetchData();
  }, [username]);

  if (loading) return <div>Loading...</div>;
  if (error) return <div>{error}</div>;

  function handleStrengthClick(strength) {
    setSelectedStrength(strength);
  }

  function handleStrengthClose() {
    setSelectedStrength(null);
  }

  return (
    <div className={`${styles.container} animate__animated animate__fadeIn`}>
      <h1>{userData.person.name}</h1>
      <p>{userData.person.professionalHeadline}</p>
      <img
        src={userData.person.picture}
        alt={userData.person.name}
        className={styles.profileImage}
      />
      <h2>Skills and Interests</h2>
      {selectedStrength && (
        <StrengthDetails
          strength={selectedStrength}
          onClose={handleStrengthClose}
        />
      )}
      {Object.entries(groupAndSortStrengths(userData.strengths)).map(
        ([proficiency, strengths]) => (
          <div key={proficiency} className={styles.group}>
            <h3>Proficiency: {proficiency}</h3>
            <ul>
              {strengths.map((strength) => (
                <li key={strength.id}>
                  <button onClick={() => handleStrengthClick(strength)}>
                    {strength.name}
                  </button>
                </li>
              ))}
            </ul>
        </div>
      ))}
    </div>
  );
}

export default UserProfile;
