// src/api.js
export async function fetchUserData(username) {
    // Replace "http://localhost:8000" with the actual domain and port number of your Django backend
    const response = await fetch(`http://localhost:8000/api/user/${username}/`);
    if (response.ok) {
      return await response.json();
    }
    throw new Error('User not found');
  }
  
  