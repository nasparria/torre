import React from 'react';
import styles from './UserProfile.module.css';

function SkillsDetails({ strength, onClose }) {
  const { name, proficiency, recommendations, hits } = strength;

  function handleClick(event) {
    if (!event.target.classList.contains(styles.popup)) {
      onClose();
    }
  }

  return (
    <div className={styles.popup} onClick={handleClick}>
      <div className={styles.popupContent}>
        <h2>{name}</h2>
        <p>Proficiency: {proficiency}</p>
        <p>Recommendations: {recommendations}</p>
        <p>Times used: {hits}</p>
        <button onClick={onClose}>Close</button>
      </div>
    </div>
  );
}

export default SkillsDetails;


