import { Switch, Route, Router} from 'react-router-dom';
import React from 'react';
import UserProfile from './UserProfile';
import { BrowserRouter } from 'react-router-dom';

function App() {
  return (
    <BrowserRouter>
      <UserProfile username="nasparria" />
    </BrowserRouter>
  );
}

export default App;
