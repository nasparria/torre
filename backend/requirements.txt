Django==3.2.10
djangorestframework==3.12.4
psycopg2-binary==2.9.3
django-cors-headers==3.10
requests==2.26.0
